//
//  Post.swift
//  NewsAPI
//
//  Created by Zulfkhar Maukey on 28/5/2020.
//  Copyright © 2020 Zulfkhar Maukey. All rights reserved.
//

import Foundation

struct Post: Codable {
    var status: String
    var totalResults: Int
    var articles: [Article]
}

struct Article: Codable {
    var source: Source
    var author: String?
    var title: String
    //var description: String
    var url: String
    var urlToImage: String?
    var publishedAt: String
    //var content: String
}

struct Source: Codable {
    //var id: String
    var name: String
}

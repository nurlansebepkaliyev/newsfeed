//
//  ArticleCached.swift
//  NewsFeed
//
//  Created by Zulfkhar Maukey on 28/5/2020.
//  Copyright © 2020 Zulfkhar Maukey. All rights reserved.
//

import Foundation
import RealmSwift

@objcMembers
class ArticleCached: Object {
    dynamic var author: String?
    dynamic var title: String?
    //var description: String
    dynamic var url: String?
    dynamic var urlToImage: String?
    dynamic var publishedAt: String?
    dynamic var sourceName: String?
    dynamic var categoryName: String?
    dynamic var saved = false
    dynamic var content: String = ""
    
    override class func primaryKey() -> String? {
        return "title"
    }
}



//
//  NotificationProvider.swift
//  NewsFeed
//
//  Created by Zulfkhar Maukey on 28/5/2020.
//  Copyright © 2020 Zulfkhar Maukey. All rights reserved.
//

import Foundation

let notificationSendArticles = Notification.Name("notification_articles")

class NotificationProvider {
    private var timer: Timer?
    
    func start() {
        timer = Timer.scheduledTimer(withTimeInterval: 60, repeats: true, block: { (timer) in
            NotificationCenter.default.post(name: notificationSendArticles, object: nil)
        })
    }
    
    func stop() {
        timer?.invalidate()
    }
}

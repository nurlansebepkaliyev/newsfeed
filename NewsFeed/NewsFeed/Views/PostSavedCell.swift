//
//  PostSavedCell.swift
//  NewsFeed
//
//  Created by Beka Zhapparkulov on 6/10/20.
//  Copyright © 2020 Beka. All rights reserved.
//

import UIKit

protocol PostTableCellDelegate {
    func saveButtonClick(post: ArticleCached)
}
class PostSavedCell: UITableViewCell {

    @IBOutlet weak var cardSavedView: UIView!
    @IBOutlet weak var postSavedImage: UIImageView!
    @IBOutlet weak var postSavedDateLabel: UILabel!
    @IBOutlet weak var savedButton: UIButton!
    @IBOutlet weak var postSavedTitleLabel: UILabel!
    var cellDelegate: PostTableCellDelegate?
    var post: ArticleCached!
    override func awakeFromNib() {
        super.awakeFromNib()
        cardSavedView.layer.cornerRadius = 20
        cardSavedView.layer.shadowColor = UIColor(red: 0.91, green: 0.91, blue: 0.91, alpha: 1).cgColor
        cardSavedView.layer.shadowOpacity = 50
        cardSavedView.layer.shadowOffset = CGSize.init(width: 0, height: 2)
        cardSavedView.layer.shadowRadius = 10
    }
    @IBAction func saveButtonClick(_ sender: Any) {
        cellDelegate?.saveButtonClick(post: post!)
    }
}

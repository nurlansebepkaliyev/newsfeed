//
//  PostCell.swift
//  NewsFeed
//
//  Created by Beka on 28/5/2020.
//  Copyright © 2020 Kazybek Zhapparkulov. All rights reserved.
//

import UIKit

protocol TableCellDelegate {
    func saveButtonClicked(post: ArticleCached)
}
class PostCell: UITableViewCell {

    @IBOutlet weak var cardNewsView: UIView!
    @IBOutlet weak var postDateLbl: UILabel!
    @IBOutlet weak var postImage: UIImageView!
    @IBOutlet weak var postTitleLbl: UILabel!
    @IBOutlet weak var postSavedButton: UIButton!
    
    var cellDelegate: TableCellDelegate?
    var post: ArticleCached!
    override func awakeFromNib() {
        super.awakeFromNib()
        cardNewsView.layer.cornerRadius = 20
        cardNewsView.layer.shadowColor = UIColor(red: 0.91, green: 0.91, blue: 0.91, alpha: 1).cgColor
        cardNewsView.layer.shadowOpacity = 50
        cardNewsView.layer.shadowOffset = CGSize.init(width: 0, height: 2)
        cardNewsView.layer.shadowRadius = 10
    }
    
    @IBAction func savedButtonClicked(_ sender: Any) {
         cellDelegate?.saveButtonClicked(post: post!)
    }
}

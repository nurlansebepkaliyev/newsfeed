//
//  SearchViewController.swift
//  NewsFeed
//
//  Created by nurlans on 6/1/20.
//  Copyright © 2020 Zulfkhar Maukey. All rights reserved.
//

import UIKit
import RealmSwift

class SearchViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchField: UITextField!
    @IBOutlet weak var findImageBackground: UIImageView!
    @IBOutlet weak var recentSearchesLabel: UILabel!
    @IBOutlet weak var clearButton: UIButton!
    
    var isTextFieldClicked = false
    var articles: [ArticleCached]?
    var recentSearch: [String] = []
    let userDefaults = UserDefaults.standard
   
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
        recentSearch = userDefaults.stringArray(forKey: "keyTitle") ?? [String]()
    }

    @IBAction func clearButton(_ sender: Any) {
        userDefaults.removeObject(forKey: "keyTitle")
        recentSearch.removeAll()
        tableView.reloadData()
    }
    @IBAction func hideKeyboardClick(_ sender: Any) {
        searchField.resignFirstResponder()
    }
    @IBAction func cancelButton(_ sender: Any) {
        isTextFieldClicked = false
        searchField.text = ""
        findImageBackground.alpha = 1.0
        recentSearchesLabel.alpha = 1.0
        clearButton.alpha = 1.0
        tableView.reloadData()
    }
    
    @IBAction func searchFieldClick(_ sender: Any) {
        isTextFieldClicked = true
        let realm = try! Realm()
        let searchingString = "*"+(searchField?.text ?? "")+"*"
        articles = Array(realm.objects(ArticleCached.self).filter("title like [c]%@ or content like [c]%@", searchingString, searchingString))
        findImageBackground.alpha = 0.0
        recentSearchesLabel.alpha = 0.0
        clearButton.alpha = 0.0
        tableView.alpha = 1.0
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isTextFieldClicked {
            return articles?.count ?? 0
        } else {
            return recentSearch.count
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchCell", for: indexPath) as! SearchTableViewCell
        cell.selectionStyle = .none
        if isTextFieldClicked {
            cell.searchTitleLabel?.text = articles?[indexPath.row].title
        } else {
            cell.searchTitleLabel?.text = recentSearch[indexPath.row]
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if isTextFieldClicked {
            performSegue(withIdentifier: "goToDetail", sender: self)
            recentSearch.append(searchField.text!)
            userDefaults.set(recentSearch, forKey: "keyTitle")
        } else {
            searchField.text = recentSearch[indexPath.row]
            searchFieldClick(self)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToDetail" {
            guard let index = tableView.indexPathForSelectedRow?.row else {fatalError("can't get index of selected cell.")}
            let destinationVC = segue.destination as! DetailViewController
            destinationVC.article = articles?[index]
        }
    }
}

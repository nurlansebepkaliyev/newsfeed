//
//  AboutViewController.swift
//  NewsFeed
//
//  Created by Beka Zhapparkulov on 6/2/20.
//  Copyright © 2020 Beka Zhapparkulov. All rights reserved.
//

import UIKit
import MessageUI

class AboutViewController: UIViewController, MFMailComposeViewControllerDelegate {

    @IBOutlet weak var profileKazybekImage: UIImageView!
    @IBOutlet weak var profileZulfkharImage: UIImageView!
    @IBOutlet weak var profileNurlanImage: UIImageView!
    
    override func viewDidLoad() {
           super.viewDidLoad()
           _ = profileKazybekImage.roundImageView(imageView: profileKazybekImage)
           _ = profileNurlanImage.roundImageView(imageView: profileNurlanImage)
           _ = profileZulfkharImage.roundImageView(imageView: profileZulfkharImage)
    }
    
    @IBAction func emailToKazybekButton(_ sender: Any) {
        messageSend(email: "kzhapparkulov@gmail.com")
    }
    @IBAction func linkedInKazybekButton(_ sender: Any) {
        guard let url = URL(string: "https://www.linkedin.com/in/kazybek-zhapparkulov/") else { return }
        UIApplication.shared.open(url)
    }
   
    @IBAction func emailToZulfkharButton(_ sender: Any) {
        messageSend(email: "zhanarbek.zulfkhar@gmail.com")
    }
    @IBAction func linkedInZulfkharButton(_ sender: Any) {
        guard let url = URL(string: "https://www.linkedin.com/in/zulfkhar-maukey-39730a1a6/") else { return }
        UIApplication.shared.open(url)
    }
    
    @IBAction func emailToNurlanButton(_ sender: Any) {
        messageSend(email: "sebepkaliyev.nurlan@gmail.com")
    }
    @IBAction func linkedInNurlanButton(_ sender: Any) {
        guard let url = URL(string: "https://www.linkedin.com/in/nurlan-sebepkaliyev-6884931a5/") else { return }
        UIApplication.shared.open(url)
    }
    
    @IBAction func goToWebsiteButton(_ sender: Any) {
        guard let url = URL(string: "https://jumysbar.kz/ios") else { return }
        UIApplication.shared.open(url)
    }
    
    @IBAction func visitInstagramButton(_ sender: Any) {
        guard let url = URL(string: "https://www.instagram.com/jumysbar_kz/") else { return }
        UIApplication.shared.open(url)
    }
    
    @IBAction func visitYoutubeButton(_ sender: Any) {
        guard let url = URL(string: "https://www.youtube.com/channel/UC5ML2YiyLqsfGL5_z5QoR_Q") else { return }
        UIApplication.shared.open(url)
    }
    
    @IBAction func visitFacebookButton(_ sender: Any) {
        guard let url = URL(string: "https://www.facebook.com/jumysbar.kz/") else { return }
        UIApplication.shared.open(url)
    }
    
    @IBAction func visitTelegramButton(_ sender: Any) {
        guard let url = URL(string: "https://t.me/jumysbarkz") else { return }
        UIApplication.shared.open(url)
    }
    
    func messageSend(email: String) {
        if MFMailComposeViewController.canSendMail() {
        let mymail = MFMailComposeViewController()
        mymail.mailComposeDelegate = self
        mymail.setToRecipients([email])
        present(mymail, animated: true)
        } else {
            print("Can not send email")
        }
        func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
            controller.dismiss(animated: true)
        }
    }
}

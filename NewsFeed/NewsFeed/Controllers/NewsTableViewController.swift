//
//  NewsTableViewController.swift
//  NewsAPI
//
//  Created by Zulfkhar Maukey on 28/5/2020.
//  Copyright © 2020 Zulfkhar Maukey. All rights reserved.
//

import UIKit
import SDWebImage
import SideMenu
import RealmSwift

class NewsTableViewController: UITableViewController, NewsProviderDelegate, MenuListDelegate, TableCellDelegate, SavedControllerDelegate {

    let realm = try! Realm()
    var articles: [ArticleCached]?
    var newsProvider = NewsProvider()
    var menu: SideMenuNavigationController?
    var hasAlreadyLaunched: Bool!
    var selectedCategory = "top-headlines?sources=the-next-web"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        newsProvider.delegate = self
        
        let menuListController = MenuListController()
        menuListController.delegate = self
        menu = SideMenuNavigationController(rootViewController: menuListController)
        menu?.leftSide = true
        
        SideMenuManager.default.leftMenuNavigationController = menu
        SideMenuManager.default.addScreenEdgePanGesturesToPresent(toView: self.view, forMenu: SideMenuManager.PresentDirection.left)
        
        articles = Array(realm.objects(ArticleCached.self).filter("categoryName == 'popular'").sorted(byKeyPath: "publishedAt", ascending: false))
        
        tableView.tableFooterView = UIView()
        //print(FileManager.default.urls(for: .documentDirectory, in: .userDomainMask))
    }
    
    override func viewDidAppear(_ animated: Bool) {
        hasAlreadyLaunched = UserDefaults.standard.bool(forKey: "hasAlreadyLaunched")
        if !hasAlreadyLaunched {
            UserDefaults.standard.set(true, forKey: "hasAlreadyLaunched")
            newsProvider.fetchArticles()
        }
        navigationController?.navigationBar.barStyle = .black
        NotificationCenter.default.addObserver(self, selector: #selector(receiveNotification), name: notificationSendArticles, object: nil)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    
    func updateUI(with category: String) {
        do {
            let realm = try Realm()
            self.articles = Array(realm.objects(ArticleCached.self).filter("categoryName == %@", category).sorted(byKeyPath: "publishedAt", ascending: false))
            self.tableView.reloadData()
        } catch {
            fatalError()
        }
    }
    
    func runSegue(identifier: String) {
        performSegue(withIdentifier: identifier, sender: self)
    }

    func reload(category: String, navigationBarTitle: String) {
        title = navigationBarTitle
        newsProvider.selectedCategory = category
        newsProvider.fetchArticles()
    }
    
    func saveButtonClicked(post: ArticleCached) {
        let realm = try! Realm()
        let article = realm.objects(ArticleCached.self).filter("title == %@", post.title!).first
        try! realm.write {
            article!.saved = !post.saved
        }
        tableView.reloadData()
    }
    
    func onSaveSuccessed() {
        tableView.reloadData()
    }
    
    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 240.0
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return articles?.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ArticleCell", for: indexPath) as! PostCell
        
        if let url = articles?[indexPath.row].urlToImage {
            cell.postImage.sd_setImage(with: URL(string: url), placeholderImage: UIImage(named: "placeholder"))
        }
        cell.postTitleLbl.text = articles?[indexPath.row].title
        cell.postDateLbl.text = decodeDate(date: (articles![indexPath.row].publishedAt)!)
        cell.postSavedButton.setImage(UIImage(named: (articles?[indexPath.row].saved)! ? "SavedIcons.png" : "Empty.png"), for: .normal)
        cell.cellDelegate = self
        cell.post = articles?[indexPath.row]
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "goToDetails", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "searchButton" {return}
        if segue.identifier == "aboutSegue" {return}
        if segue.identifier == "savedSegue" {
            let dVC = segue.destination as! SavedTableViewController
            dVC.delegate = self
            return
        }
        guard let index = tableView.indexPathForSelectedRow?.row else {fatalError("can't get index of selected cell.")}
        let destinationVC = segue.destination as! DetailViewController
        destinationVC.article = articles?[index]
    }
    
    @IBAction func didTapMenu(_ sender: Any) {
        present(menu!, animated: true)
    }
    
    @objc func receiveNotification(notification: Notification) {
        newsProvider.fetchArticles()
    }
}

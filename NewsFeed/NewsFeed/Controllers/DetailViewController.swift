//
//  ViewController.swift
//  NewsFeed
//
//  Created by Beka on 28/5/2020.
//  Copyright © 2020 Kazybek Zhapparkulov. All rights reserved.
//

import UIKit
import SwiftSoup
import SDWebImage
import JGProgressHUD
import RealmSwift

class DetailViewController: UIViewController {
  
    var article: ArticleCached?
    let hud = JGProgressHUD(style: .dark)
    
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var postImageView: UIImageView!
    @IBOutlet weak var authorsLabel: UILabel!
    @IBOutlet weak var sourceLabel: UILabel!
    @IBOutlet weak var postTitleLabel: UILabel!
    @IBOutlet weak var shareButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = article?.categoryName?.capitalizingFirstLetter()
        hud.textLabel.text = "Loading"
        hud.show(in: self.view)
        DispatchQueue.main.async {
            self.parseHTML()
            self.hud.dismiss()
        }
        let urlImage = self.article?.urlToImage
        postImageView.sd_setImage(with: URL(string: urlImage ?? ""), placeholderImage: UIImage(named: "placeholder"))
        authorsLabel.text = article?.author
        sourceLabel.text = article?.sourceName
        postTitleLabel.text = article?.title
    }
    
    @IBAction func visitPostUrlButton(_ sender: Any) {
        guard let url = URL(string: (article?.url)!) else { return }
        UIApplication.shared.open(url)
    }
    
    func parseHTML() {
        guard let urlString = article?.url else {fatalError("Cannot get url string.")}
        guard let url = URL(string: urlString) else {fatalError("Cannot get url.")}
        do {
            let htmlString = try String(contentsOf: url)
            let html = try SwiftSoup.parse(htmlString)
            let p = try html.getElementsByTag("p").text()
            textView.text = p
        } catch {
            fatalError("error parsing html.")
        }
        
        let realm = try! Realm()
        let post = realm.objects(ArticleCached.self).filter("title == %@", article!.title!).first
        try! realm.write {
            post!.content = textView.text
        }
    }
    
    @IBAction func shareButton(_ sender: Any) {
        let urlString = article?.url
        let postTitle = article?.title
        let items: [Any] = [postTitle!, URL(string: urlString!)!]
        let activityController = UIActivityViewController(activityItems: items, applicationActivities: nil)
        present(activityController, animated: true)
    }
}

extension String {
    func capitalizingFirstLetter() -> String {
        return prefix(1).capitalized + dropFirst()
    }

    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
}

//
//  ViewController.swift
//  NewsFeed
//
//  Created by Zulfkhar Maukey on 28/5/2020.
//  Copyright © 2020 Zulfkhar Maukey. All rights reserved.
//

import UIKit
import SwiftSoup

class DetailViewController: UIViewController {
    
    var article: ArticleCached?

    @IBOutlet weak var textView: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()

        parseHTML()
    }
    
    func parseHTML() {
        guard let urlString = article?.url else {fatalError("Cannot get url string.")}
        guard let url = URL(string: urlString) else {fatalError("Cannot get url.")}
        do {
            let htmlString = try String(contentsOf: url)
            let html = try SwiftSoup.parse(htmlString)
            let p = try html.getElementsByTag("p").text()
            textView.text = p
        } catch {
            fatalError("error parsing html.")
        }
    }
    

}
